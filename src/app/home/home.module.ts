import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import các thư viện ngPrime
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button'

// Import module font-awesome
// import {AngularFontAwesomeModule} from 'angular-font-awesome'

import { HomeRoutingModule } from './home-routing.module';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { TemplateComponent } from './template/template.component';
import { ShareModule } from '../share/share.module';
// import { MainModule } from '../main/main.module';
import { TaskDetailComponent } from './task-detail/task-detail.component';


@NgModule({
  declarations: [
    TaskListComponent,
    TaskItemComponent,
    TemplateComponent,
    TaskDetailComponent,
    
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    // MainModule,
    ShareModule,
    CardModule,
    TableModule,
    ButtonModule,
    // AngularFontAwesomeModule
  ],
  
})
export class HomeModule { }
