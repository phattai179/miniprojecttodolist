import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { filter } from 'rxjs/operators';
import { TaskQuery } from 'src/app/akita/query';
import { TaskStore } from 'src/app/akita/store';
import { ModalFormService } from 'src/app/core/services/modal-form.service';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {

  @Input() task: any;
  @Output() onDelete = new EventEmitter ()
  @Output() onViewTask = new EventEmitter ()

  constructor(
    private modal: ModalFormService,
    private taskService: TaskManagerService,
    private taskQuery: TaskQuery,
    private taskStore: TaskStore
  ) { }

  ngOnInit(): void {
  }

  deleteTask = (id: any) => {
    // this.onDelete.emit(id)
    
  }

  editTask = (taskEdit: any) => {
    this.modal.statusModal.next(true)
    this.modal.contentHeader.next('Edit Task')
  }

  handleViewTask = () => {
    // console.log('taskDetail', this.task)
    this.onViewTask.emit(this.task)
  }

}




