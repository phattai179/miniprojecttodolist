import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, take, filter } from 'rxjs/operators'
import { TaskQuery } from 'src/app/akita/query';
import { TaskStore } from 'src/app/akita/store';
import { Task } from 'src/app/core/models/task';
import { ModalFormService } from 'src/app/core/services/modal-form.service';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';
import { HomeService } from '../home.service';
import { TaskDetailComponent } from '../task-detail/task-detail.component';



@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  @ViewChild('taskDetail') taskDetailComponent !: TaskDetailComponent

  loading: boolean = false
  tasks: Observable<any> | undefined
  tasks$: Observable<any> | undefined

  taskList: Task[] = []

  // Khai báo biến của table Ngprime
  first = 0;

  rows = 4;

  constructor(
    private modal: ModalFormService,
    private taskService: TaskManagerService,
    private taksHomeService: HomeService,
    private taskQuery: TaskQuery,
    private taskStore: TaskStore,
  ) { }

  ngOnInit(): void {
    // Dựa vào các query theo dõi các biến, mảng ... ở trên store rồi cập nhật lại ở component
    this.taskQuery.getLoading().subscribe(res => this.loading = res)
    // this.taskQuery.getTasks().subscribe(res => this.tasks = res);
    // // Sau khi lấy task thì không cần subscribe mà load thẳng ra giao diện
    // // this.tasks$ = this.taskQuery.select();
    // this.tasks$ = this.taskQuery.getTasks();
    // //.subscribe(res => this.tasks = res);
    this.tasks = this.taskQuery.getTasks()

    let taskOb: Observable<any> = this.taskQuery.getLoaded()
    // C1 Hàm getTaskHome trong file homeService để xử lý các logic gọi api lấy danh sách task và update lại trên store akita
    this.taksHomeService.getTaskHome(taskOb)


    // Cách 2 làm việc với service để quản lý state taskList
    this.taskService.getTaskList().subscribe({
      next: (res) => {
        this.taskService.taskList.next(res)
      }
    })

    this.taskService.taskList.asObservable().subscribe({
      next: (res) => {
        this.taskList = res
      }
    })
  }


  deleteTask = (task : any) => {
    console.log('deletag')
  }

  handleViewTask = (taskView: any) => {
    this.taskDetailComponent.taskDetail = taskView
  }

  showBasicModal = () => {
    this.modal.statusModal.next(true)
    this.modal.contentHeader.next('Add Task')
  }


  // logic của table Ngprime (copy ngPrime)
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {

    let taskPage: any = ""

    this.tasks?.subscribe(res => taskPage ? this.first === res.legnth - this.rows : true)

    return taskPage
  }

  isFirstPage(): boolean {
    return this.tasks ? this.first === 0 : true;
  }

}
