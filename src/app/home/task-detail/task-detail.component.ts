import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TaskQuery } from 'src/app/akita/query';
import { TaskStore } from 'src/app/akita/store';
import { ModalFormService } from 'src/app/core/services/modal-form.service';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';
import { ConfirmPopupComponent } from 'src/app/share/confirm-popup/confirm-popup.component';
import { ModalFormComponent } from 'src/app/share/modal-form/modal-form.component';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {


  @ViewChild ('formEdit') formEditComponent !: ModalFormComponent
  // @Input () taskDefault : any
  taskDetail: any = {}
  constructor(
    private modal: ModalFormService,
    private taskService: TaskManagerService,
    private taskQuery: TaskQuery,
    private taskStore: TaskStore
  ) { }

  ngOnInit(): void {
    this.taskQuery.getTasks().subscribe({
      next: (res) => {
        this.taskDetail = res[0]
      }
    })
    // console.log('taskDetail', this.taskDetail)
  }


  editTask = (taskEdit: any) => {
    this.modal.statusModal.next(true)
    this.modal.contentHeader.next('Edit Task')

    console.log('taskEdit', taskEdit)
    this.formEditComponent.taskIdUpdate = taskEdit.id
    this.formEditComponent?.form.setValue({
      name : taskEdit.name,
      user: taskEdit.user,
      description: taskEdit.description,
      time: taskEdit.time
    })

  }

  

}
