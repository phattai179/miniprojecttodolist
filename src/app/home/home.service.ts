import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { TaskQuery } from '../akita/query';
import { TaskStore } from '../akita/store';
import { TaskManagerService } from '../core/services/task-manager.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  tasks$: Observable<any> | undefined

  constructor(
    private taskQuery: TaskQuery,
    private taskStore: TaskStore,
    private taskService: TaskManagerService
  ) { }


  getTaskHome = (taskOb : Observable<any>) => {
    // this.taskQuery.getLoaded().pipe
      taskOb.pipe(
      take(1),
      filter(res => !res),
      switchMap(() => {
        this.taskStore.setLoading(true)
        // Gọi hàm getTask để lấy api danh sách task ở file service
        return this.taskService.getTaskList()
      })
    ).subscribe({
      next: (res) => {
        // Cập nhật lại task trên store trên Akita
        this.taskStore.update(state => {
          return {tasks: res}
        })
        this.taskStore.setLoading(false)
      },
      error: (err) => {
        console.log(err)
        this.taskStore.setLoading(false)
      }
    })

    // this.tasks$ = this.taskQuery.select();
    // Sau khi lấy task thì không cần subscribe mà load thẳng ra giao diện
    // this.tasks$ = this.taskQuery.getTasks();
    //.subscribe(res => this.tasks = res);
  }

}
