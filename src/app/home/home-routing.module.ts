import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskListComponent } from './task-list/task-list.component';
import { TemplateComponent } from './template/template.component';
// import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  {path: "", component: TemplateComponent,
    children: [
      {path: "", redirectTo: "tasklist", pathMatch: 'prefix' },
      {path: "tasklist", component: TaskListComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
