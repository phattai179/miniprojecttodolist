import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './core/guards/admin.guard';

const routes: Routes = [
 
  {
    path: "admin", 
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
    canActivate : [AdminGuard]
  },
  {path: "home", loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)},
  {path: "", loadChildren: () => import('./main/main.module').then((m) => m.MainModule )},
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }




