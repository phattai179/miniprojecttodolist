import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MainRoutingModule } from '../main/main-routing.module';
import { ModalFormComponent } from './modal-form/modal-form.component';

// Import formModule 
import {FormsModule} from '@angular/forms'

// Import các module trong Ngprime
import {DialogModule} from 'primeng/dialog'
import {ButtonModule} from 'primeng/button';
import {ConfirmPopupModule} from 'primeng/confirmpopup'
import { ToastModule } from "primeng/toast";
import {ConfirmationService, MessageService} from 'primeng/api'

import { ConfirmPopupComponent } from './confirm-popup/confirm-popup.component'

@NgModule({
  declarations: [
    HeaderComponent,
    ModalFormComponent,
    ConfirmPopupComponent,

  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    DialogModule,
    ButtonModule,
    ConfirmPopupModule,
    ToastModule,

    FormsModule
  ],
  exports: [
    HeaderComponent,
    ModalFormComponent,
    ConfirmPopupComponent
  ],
  providers: [ConfirmationService, MessageService]
})
export class ShareModule { }
