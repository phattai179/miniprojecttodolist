import { Component, Input, OnInit } from '@angular/core';

import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api'
import { TaskStore } from 'src/app/akita/store';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';


@Component({
  selector: 'app-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.scss']
})
export class ConfirmPopupComponent implements OnInit {
  @Input () keyId : any 

  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private taskService: TaskManagerService,
    private taskStore: TaskStore,
  ) { }

  confirm(event: Event | any) {
    this.confirmationService.confirm({
      key: this.keyId,
      target : event.target,
      message: "Are you sure that you want to delete?",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.messageService.add({
          severity: "info",
          summary: "Confirmed",
          detail: "You have accepted"
        });

        // Gọi lên service để gọi apidelete Task
        this.taskService.deleteTask(this.keyId).subscribe({
          next: (res) => {
            // Update lại dữ liệu lên store
            this.taskStore.update(state => {
              return {
                ...state,
                tasks: state.tasks.filter(item => item.id !== res.id)
              }
            })

            // C2 cập nhật xóa taskList trên service
            let taskListDeleteItem = this.taskService.taskList.value.filter(item => item.id !== res.id)
            this.taskService.taskList.next(taskListDeleteItem)

          },
          error: (err) => {
            console.log(err)
          }
        })


      },
      reject: () => {
        this.messageService.add({
          severity: "error",
          summary: "Rejected",
          detail: "You have rejected"
        });
      }
    });
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

}
