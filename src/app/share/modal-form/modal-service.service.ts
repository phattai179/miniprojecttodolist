import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskStore } from 'src/app/akita/store';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';

@Injectable({
  providedIn: 'root'
})
export class ModalServiceService {

  constructor(
    private taskStore : TaskStore,
    private taskService: TaskManagerService

  ) { }


  addTaskService = (taskOb: Observable<any>) => {
    taskOb.subscribe({
      next:(res) => {
        // C1 Cập nhật lại store trên Akita
        this.taskStore.update(state => {
          return {tasks: [...state.tasks, res]}
        })
        
        // C2: Luyện tập với xử lý state trên service
        this.taskService.taskList.next([...this.taskService.taskList.value, res])
      }
    })
  }

  editTaskService = (taskOb: Observable<any>) => {
    taskOb.subscribe({
      next: (res) => {
        // C1: Cập nhật xử lý update mới trên storeAkita
        this.taskStore.update(state => {
          let taskUpdate: any[] = [...state.tasks]
          let index = taskUpdate.findIndex(item => item.id === res.id)
          taskUpdate[index] = res
          return {tasks: taskUpdate}
        })

        // C2 Cập nhật lại task để quản lý bằng state trên service
        let taskServiceUpdate = [...this.taskService.taskList.value]

        let indexItemUpdate = taskServiceUpdate.findIndex(item => item.id === res.id)

        taskServiceUpdate[indexItemUpdate] = res
        this.taskService.taskList.next(taskServiceUpdate)

        this.taskStore.setLoading(false)

      },
      error : (err) => {
        console.log(err)
        this.taskStore.setLoading(false)
      }
    })
  }

}
