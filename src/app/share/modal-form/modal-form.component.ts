import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms'
import { PrimeNGConfig } from 'primeng/api';
import { TaskQuery } from 'src/app/akita/query';
import { TaskStore } from 'src/app/akita/store';
import { ModalFormService } from 'src/app/core/services/modal-form.service';
import { TaskManagerService } from 'src/app/core/services/task-manager.service';
import { ModalServiceService } from './modal-service.service';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {



  @ViewChild('formModal') form !: NgForm
  constructor(private primengConfig: PrimeNGConfig,
    private modal: ModalFormService,
    private taskService: TaskManagerService,
    private taskQuery: TaskQuery,
    private taskStore: TaskStore,
    private modalService: ModalServiceService
  ) { }

  displayModal: boolean = false;
  displayBasic: boolean = false;
  kindHeader: any = null;

  taskIdUpdate: any = ""


  ngOnInit(): void {
    this.primengConfig.ripple = true
    // Logic hiển thị modalForm khi nhấn edit hoặc add
    this.modal.statusModal.asObservable().subscribe({
      next: (result) => {
        this.displayBasic = result
      }
    })
    this.modal.contentHeader.asObservable().subscribe({
      next: (result) => {
        this.kindHeader = result
      }
    })
  }

  showBasicDialog() {
    this.modal.statusModal.next(true)
  }

  
  handleAddTask = () => {
    this.modal.statusModal.next(false)
    this.taskStore.setLoading(true);
    // Thực hiện gọi hàm add task trên file service

    let taskOb = this.taskService.addTask(this.form.value)
    // File modalServie ở chính component xứ lý logic
    this.modalService.addTaskService(taskOb)

    this.form.reset()
  }

  handleUpdateTask = () => {
    this.modal.statusModal.next(false)

    let taskUpdate = this.form.value
    this.taskStore.setLoading(true)

    // Gọi đến file service tổng để gọi api editTask

    let taskOb = this.taskService.editTask(taskUpdate, this.taskIdUpdate)

    // Hàm editTaskService để xử lý logic sau khi update Task trên state Akita sau khi gọi trả kết quả từ api
    this.modalService.editTaskService(taskOb)

    this.form.reset()
  }

  closeModal = () => {
    this.modal.statusModal.next(false)
    this.form.reset()
  }

}
