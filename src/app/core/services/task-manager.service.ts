import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs'
import {HttpClient} from '@angular/common/http'
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskManagerService {

  constructor( private http: HttpClient,) { }

  taskList = new BehaviorSubject<Task[]>([])


  getTaskList = (): Observable<Task[]> => {
    const url = `https://60a0dadad2855b00173b14a3.mockapi.io/api/tasks`

    return this.http.get<Task[]>(url)
  }

  addTask = (task: any): Observable<any> => {
    const url = `https://60a0dadad2855b00173b14a3.mockapi.io/api/tasks`
    console.log('task', task)
    return this.http.post<any>(url, task)
  }

  editTask = (task: any, id: any): Observable<any> => {
    const url = `https://60a0dadad2855b00173b14a3.mockapi.io/api/tasks/${id}`

    return this.http.put<any>(url, task)
  }

  deleteTask = (id : any) : Observable<any> => {
    const url = `https://60a0dadad2855b00173b14a3.mockapi.io/api/tasks/${id}`
    
    return this.http.delete<any>(url)

  }

}
