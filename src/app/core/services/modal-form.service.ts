import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ModalFormService {


  statusModal = new BehaviorSubject<any>(false)
  contentHeader = new BehaviorSubject<any>(null)

  handleShowModal = (status: boolean) => {
    console.log('status', status)
  }




  constructor() { }
}
