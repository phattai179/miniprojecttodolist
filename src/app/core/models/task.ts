
export interface Task {
    id: string,
    name: string,
    user: string,
    description: string,
    time: string
}