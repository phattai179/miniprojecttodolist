import { Component, OnInit, ViewChild } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import {NgForm} from '@angular/forms'
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild ('formDangNhap') formDN !: NgForm

  constructor(
    private primengConfig: PrimeNGConfig,
    private router: Router,
    private userService: UserService,

    ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true

  }

  handleLogin = () => {    
    if(this.formDN.invalid){
      return
    }

    console.log('formDangKy', this.formDN.value)
    this.userService.userLocal.next(this.formDN.value)

    alert('Đăng nhập thành công')
    this.router.navigateByUrl('/home')
  }

}
