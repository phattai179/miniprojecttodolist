import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import formsModules
import {FormsModule} from '@angular/forms'

// import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import { MainRoutingModule } from './main-routing.module';
import { LoginComponent } from './login/login.component';
import { RippleModule } from 'primeng/ripple';
import { SharedModule } from 'primeng/api';


@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,

    // ngPrime
    InputTextModule,
    ButtonModule,
    // BrowserAnimationsModule,
    RippleModule,
    // Share
    SharedModule,
    // Import formModule
    FormsModule
  ],
  exports: [
    LoginComponent,

    InputTextModule,
    ButtonModule,
    // BrowserAnimationsModule,
    RippleModule,
  ]
})
export class MainModule { }
