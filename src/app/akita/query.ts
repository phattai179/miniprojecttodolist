import { Injectable } from '@angular/core';
import {Query} from '@datorama/akita'
import { Observable } from 'rxjs';
import { Task } from '../core/models/task';
import { TaskStore, TaskState } from './store';


@Injectable({
    providedIn: 'root'
})

export class TaskQuery extends Query<TaskState> {
    constructor(private taskStore: TaskStore){

        super(taskStore)
    }

    getTasks(): Observable<Task[]> {
        return this.select(state => state.tasks)
    }

    getLoaded(): Observable<boolean> {
        return this.select(state => state.isLoaded)
    }   

    getLoading(): Observable<boolean> {
        return this.selectLoading()
    }

}




